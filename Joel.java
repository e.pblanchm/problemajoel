package Exercici;

import java.util.Scanner;

public class Joel {

    public static void main(String[] args) {
        programa();
    }

    public static void programa() {
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 9; i++) {
            funcio(sc);
        }
    }

    public static void funcio(Scanner sc) {
        String paraula = sc.nextLine();
        if (paraula.length() == 9 && paraula.contains("p") && paraula.contains("a") &&
            paraula.contains("l") && paraula.contains("o") && paraula.contains("n")) {
            System.out.println("OK");
        } else {
            System.out.println("NO");
        }
    }
}

		    /*La eruga
		     * tenim una mascota molt simpàtica anomenada caterpillar les erugues són uns  animals fascinants 
		     * dels pocs capaços de poder-se metamorfosar el problema és que la nostra mascota no ho pot fer fins que 
		     * sàpiga ven bé com es dirà el nou animal que serà. Ell sap que és una d'aquestes opcions però no sap l'ordre de 
		     * les lletres podries donar-li un cop de mà? Fes un programa que doni OK quan es pugui formar 
		     * la paraula papallona si la paraula no es pot formar la paraula papallona dirà NO el  primer et donarem el nombre de casos i després les paraules possibles
		     * ENTRADA:papalleona,  papafrita, onllapapa, papallorona, butterfree, palolonpa de dir OK quan tingui exactament les mateixes lleltres
		     * SORTIDA: NO					NO 		OK			NO				NO			NO	
9		     * 	
papalleona
papafrita
onllapapa
papallorona
butterfree
palolonpal
palopapal
papalleo
mamalleona*/