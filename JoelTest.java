package Exercici;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

public class JoelTest {

    @Test
    void testFuncioOK() {
        String input = "papallona";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Scanner sc = new Scanner(System.in);
        Joel.funcio(sc);
        assertEquals("OK", outContent.toString().trim());
    }

    @Test
    void testFuncioNO() {
        String input = "papapapanananlalan";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Scanner sc = new Scanner(System.in);
        Joel.funcio(sc);
        assertEquals("NO", outContent.toString().trim());
    }
}
